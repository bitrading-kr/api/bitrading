/**
 * @fileOverview 그룹 API
 * @name user.js
 * @author 2018 https://bitrading.kr
 * @copyright 2018 https://bitrading.kr
 * @created 2018-10-24
 */

"use strict";
const { TABLE } = require('./constants.js'),
      queryHelper = require('./queryHelper.js'),
      assertArgs = require('assert-args'),
      objectDefaults = require('object-defaults'),
      crypto = require('crypto'),
      bs58 = require('bs58'),
      moment = require('moment');

class Group {
  constructor (dbconnection) {
    this.db = dbconnection;
    this.helper = new queryHelper(this.db);
  }

  /**
   * @description 그룹 추가, 그룹 정보 추가, 그룹 초대코드 추가
   *
   * 3 insert queries
   */
  async add (...args) {
    let options = assertArgs(args, {
      name: 'string',
      email: 'string',
      '[description]': 'string',
      '[homepage]': 'string',
      '[registered]': 'string'
    });
    options = objectDefaults(options, {
      description: '',
      homepage: '',
      registered: moment().format('YYYY-MM-DD HH:mm:ss'),
    });

    let group_params = {
      'name': options.name
    };
    let groupId = await this.helper.insertUniq(['name'], TABLE.GROUP, group_params);
    groupId = groupId[0];

    let group_membership_params = {
      GID: groupId,
      email: options.email,
      description: options.description,
      homepage: options.homepage,
      registered: options.registered
    };
    await this.helper.insert(TABLE.GROUP_MEMBERSHIP, group_membership_params);

    let group_invitation_code_params = {
      GID: groupId,
      code: bs58.encode(crypto.randomBytes(50)),
      expired_date: moment().add(7, 'd').format('YYYY-MM-DD HH:mm:ss')
    };
    await this.helper.insert(TABLE.GROUP_INVITATION_CODE, group_invitation_code_params);

    return groupId;
  }

  async get (...args) {
    let options = assertArgs(args, {
      name: 'string'
    });

    let params = {
      name: options.name
    };

    let res = await this.db.select('*').from(TABLE.GROUP).where(params);

    return res[0];
  }

  async getId (...args) {
    let options = assertArgs(args, {
      name: 'string'
    });

    let params = {
      name: options.name
    };

    return await this.helper.selectId(TABLE.GROUP, params);
  }

  async updateName (...args) {
    let options = assertArgs(args, {
      name: 'string',
      newName: 'string'
    });

    return await this.db(TABLE.GROUP).where({name: options.name}).update({name: options.newName});
  }

  async updateMebership (...args) {
    let options = assertArgs(args, {
      name: 'string'
    });
  }

  async renewInvitationCode (...args) {
  }

  async getInvitationCode (...args) {
  }

}
