"use strict";
const fs = require('fs'),
      path = require('path'),
      mysql = require('mysql'),
      createTable = require('./createTable'),
      dropTable = require('./dropTable'),
      QueryHelper = require('../queryHelper');

const standaloneTableNames = ['user', 'group', 'license', 'market', 'coin', 'exchange', 'uuid'
                             ];
const depsTableNames = ['user_membership', 'user_access', 'user_license', 'user_api', 'user_group', 'group_membership', 'group_invitation_code', 'exchange_market', 'exchange_coin', 'order', 'trading', 'entry_strategy', 'target_strategy', 'trailing_stop_strategy'
                       ];
const createTableNames = standaloneTableNames.concat(depsTableNames);
const dropTableNames = [].concat(createTableNames).reverse();

class Init {
  constructor (dbConnection) {
    this.db = dbConnection;
    // this.helper = new QueryHelper(this.db);
    this.createTable = new createTable(this.db);
    this.dropTable = new dropTable(this.db);
  }

  async createold (tableNames) {
    let tablePaths = tableNames.map(tableName => {
      return path.resolve(__dirname, `../assets/create/${tableName}.sql`);
    });
    let results = [];
    for (var i = 0; i < tablePaths.length; ++i) {
      let result;
      try {
        result = await this.helper.import(tablePaths[i]);
      } catch (err) {
        console.log(i);
        console.log(tablePaths[i]);
        throw err;
      }
      results.push(result);
    }
    return results;
  }

  async dropold (tableNames) {
    let tablePaths = tableNames.map(tableName => {
      return path.resolve(__dirname, `../assets/drop/${tableName}.sql`);
    });
    let results = [];
    for (var i = 0; i < tablePaths.length; ++i) {
      let result = await this.helper.import(tablePaths[i]);
      results.push(result);
    }
    return results;
  }

  async create (tableNames) {
    let results = [];
    for (var i = 0; i < tableNames.length; ++i) {
      let result = await this.createTable[tableNames[i]]();
      results.push(result);
    }
    return results;
  }

  async drop (tableNames) {
    let results = [];
    for (var i = 0; i < tableNames.length; ++i) {
      let result = await this.dropTable[tableNames[i]]();
      results.push(result);
    }
    return results;
  }

  init () {
    return new Promise((resolve, reject) => {
      this.create(createTableNames).then(res => resolve(res)).catch(err => reject(err));
    });
  }

  destory () {
    return new Promise((resolve, reject) => {
      this.drop(dropTableNames).then(res => resolve(res)).catch(err => reject(err));
    });
  }

  logging (msg) {
  }
}

module.exports = Init;
