/**
 * @fileOverview 사용자 API
 * @name user.js
 * @author 2018 https://bitrading.kr
 * @copyright 2018 https://bitrading.kr
 * @created 2018-10-15
 */

"use strict";
const { TABLE } = require('./constants.js'),
      queryHelper = require('./queryHelper.js'),
      assertArgs = require('assert-args'),
      objectDefaults = require('object-defaults'),
      crypto = require('crypto'),
      moment = require('moment');
class User {
  constructor (dbconnection) {
    this.db = dbconnection;
    this.helper = new queryHelper(this.db);
  }

  getUserInfo (loginId) {
  }

  encrypt (...args) {
    let options = assertArgs(args, {
      'str': 'string'
    });

    return crypto.createHash('sha256').update(options.str).digest('hex');
  }

  async get (...args) {
    let options = assertArgs(args, {
      'loginId': 'string'
    });

    let params = {
      'login_id': options.loginId
    };

    // 사용저 전체 정복 가져오기
    let res = await this.db.select('*').from(TABLE.USER).where(params);

    return res[0];
  }

  async getId (...args) {
    let options = assertArgs(args, {
      'loginId': 'string'
    });

    let params = {
      'login_id': options.loginId
    };

    return await this.helper.selectId(TABLE.USER, params);
  }

  async add (...args) { // 이 함수는 내부적으로 사용할 것(backend에서는 쓰지 말 것.)
    let options = assertArgs(args, {
      loginId: 'string',
      loginPw: 'string',
      '[email]': 'string',
      '[registered]': 'string',
      '[status]': 'number'
    });
    options = objectDefaults(options, {
      email: '',
      registered: moment().format('YYYY-MM-DD HH:mm:ss'),
      status: 0
    });
    options.loginPw = this.encrypt(options.loginPw); // 해시화

    // 사용자 등록
    let userParams = {
      'login_id': options.loginId,
      'login_pw': options.loginPw
    };
    let userId = await this.helper.insertUniq(['login_id'], TABLE.USER, userParams);
    userId = userId[0];

    // 사용자 정보 등록
    let user_membership = {
      'UID': userId,
      'email': options.email,
      'registered': options.registered,
      'status': options.status
    };
    await this.helper.insert(TABLE.USER_MEMBERSHIP, user_membership);

    return userId;
  }

  async addAccess (...args) {
    let options = assertArgs(args, {
      'UID': ['string', 'number'],
      'access_key': 'string',
      'secret_key': 'string',
      '[expired_date]': 'string'
    });
    options = objectDefaults(options, {
      expired_date: moment().add(7, 'd').format('YYYY-MM-DD HH:mm:ss')
    });

    await this.helper.insert(TABLE.USER_ACCESS, options);

    return {
      access_key: options.access_key,
      secret_key: options.secret_key,
      expired_date: options.expired_date
    };
  }

  async updateAccess (...args) {
    let options = assertArgs(args, {
      'UID': ['string', 'number'],
      'access_key': 'string',
      'secret_key': 'string',
      '[expired_date]': 'string'
    });
    options = objectDefaults(options, {
      expired_date: moment().add(7, 'd').format('YYYY-MM-DD HH:mm:ss')
    });

    let set = {
      access_key: options.access_key,
      secret_key: options.secret_key,
      expired_date: options.expired_date
    },
        where = {
          UID: options.UID
        };
    await this.helper.update(TABLE.USER_ACCESS, set, where);

    return {
      access_key: set.access_key,
      secret_key: set.secret_key,
      expired_date: set.expired_date
    };
  }

  async getAccess (...args) {
    let options = assertArgs(args, {
      'UID': ['string', 'number']
    });

    let params = {
      UID: options.UID
    };

    let res = await this.db('*').from(TABLE.USER_ACCESS).where(params);
    return res[0];
  }

  login (loginId, loginPw) {
  }
}

module.exports = User;
