/**
 * @fileOverview 로그인 API
 * @name Auth.js
 * @author 2018 https://bitrading.kr
 * @copyright 2018 https://bitrading.kr
 * @created 2018-10-17
 */

"use strict";
const { TABLE } = require('./constants.js'),
      queryHelper = require('./queryHelper.js'),
      assertArgs = require('assert-args'),
      objectDefaults = require('object-defaults'),
      crypto = require('crypto'),
      bs58 = require('bs58'),
      { decode, sign, verify } = require('jsonwebtoken'),
      moment = require('moment-timezone'),
      momentHelper = require('./momentHelper.js'),
      User = require('./User.js');

class Auth {
  constructor (dbconnection) {
    this.db = dbconnection;
    this.helper = new queryHelper(this.db);
    this.user = new User(dbconnection);
  }
  /*
   * Access
   */
  genAccessData () {
    return {
      accessKey: bs58.encode(crypto.randomBytes(36)),
      secretKey: bs58.encode(crypto.randomBytes(36))
    };
  }

  convAccessData (dbAccessData) {
    return {
      accessKey: dbAccessData.access_key,
      secretKey: dbAccessData.secret_key,
      expiredDate: dbAccessData.expired_date
    };
  }

  async getAccess (...args) {
    let options = assertArgs(args, {
      loginId: 'string'
    });

    let { id } = await this.user.get(options.loginId);

    let res = await this.user.getAccess(id);

    return this.convAccessData(res);
  }

  async addAccess (...args) {
    let options = assertArgs(args, {
      loginId: 'string'
    });

    let { id } = await this.user.get(options.loginId);

    let accessData = this.genAccessData();

    let res = await this.user.addAccess(id,
                                        accessData.accessKey,
                                        accessData.secretKey);

    return this.convAccessData(res);
  }

  async renewAccess (...args) {
    let options = assertArgs(args, {
      loginId: 'string'
    });

    let { id } = await this.user.get(options.loginId);

    let accessData = this.genAccessData();

    let res = await this.user.updateAccess(id,
                                           accessData.accessKey,
                                           accessData.secretKey);

    return this.convAccessData(res);
  }

  async issueAccess (...args) {
    let options = assertArgs(args, {
      loginId: 'string'
    });

    let accessData;
    try {
      accessData = await this.getAccess(options.loginId);
    } catch (e) {
      // accessData
      try {
        accessData = await this.addAccess(options.loginId);
      } catch (_e) {
        throw new Error('Access 데이터에 접근하는데 오류가 발생하였습니다.');
      }
    }

    // Access Key 만료
    if (momentHelper.isExpired(accessData.expiredDate)) {
      accessData = await this.renewAccess(options.loginId);
    }

    return accessData;
  }

  /*
   * Token
   */
  genToken (...args) {
    let options = assertArgs(args, {
      loginId: 'string',
      accessKey: 'string',
      secretkey: 'string'
    });

    let token = sign({
      id: options.loginId,
      access: options.accessKey
    }, options.secretkey, {
      expiresIn: 60*60*24*7 // 7day
    });

    return token;
  }

  async issueToken (...args) {
    let options = assertArgs(args, {
      loginId: 'string'
    });

    let accessData = await this.issueAccess(options.loginId);

    return this.genToken(options.loginId,
                         accessData.accessKey,
                         accessData.secretKey);
  }

  async verifyToken (...args) {
    let options = assertArgs(args, {
      token: 'string'
    });

    let payload = decode(options.token);
    let accessData = await this.issueAccess(payload.id);

    try {
      verify(options.token, accessData.secretKey);
      return true;
    } catch (e) {
      return false;
    }
  }

  /*
   * signin
   */

  async signin (...args) {
    let options = assertArgs(args, {
      loginId: 'string',
      loginPw: 'string',
      '[email]': 'string',
      '[registered]': 'string',
      '[status]': 'number'
    });

    let accessData = this.genAccessData();

    await this.user.add(options.loginId,
                        options.loginPw,
                        options.email,
                        options.registered,
                        options.status);

    return await this.issueToken(options.loginId);
  }

  async login (...args) {
    let options = assertArgs(args, {
      loginId: 'string',
      loginPw: 'string'
    });

    // 로그인 정보 가져오기
    let res = await this.user.get(options.loginId);
    let enpw = this.user.encrypt(options.loginPw);

    if (res == null || typeof(res.login_pw) != 'string' || res.login_pw != enpw) {
      throw new Error("아이디 혹은 비밀번호가 잘못되었습니다.");
    }

    return await this.issueToken(options.loginId);
  }

  async isLogin (...args) { // token authorization
    let options = assertArgs(args, {
      token: 'string'
    });

    return await this.verifyToken(options.token);
  }

  async logout () {
  }
}

module.exports = Auth;
