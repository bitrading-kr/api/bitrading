// test for nodejs >= 10.0

require('dotenv').config();
const oneMocha = require('one-mocha');
const Bitrading = require('./index.js');
const { TABLE } = require('./lib/constants.js');
const _ = require('lodash');
const knex = require('knex');

const coinList = require('./coinList.json').upbit;
const btr = new Bitrading(process.env.DB_HOST, process.env.DB_PORT, process.env.DB_USER, process.env.DB_PASSWD, process.env.DB_DB);

const assert = require('assert');
describe(`#.Ready for test()`, function () {
  describe(`#.uninstall()`, function () {
    it(`Not Throw`, async function () {
      await assert.doesNotReject(btr.uninstall());
    });
  });
});

describe(`#.install()`, function () {
  it(`Not Reject`, async function () {
    await assert.doesNotReject(btr.install());
  });
  it(`Check created tables`, async function () {
    let tables = await btr.db.select('*').from('information_schema.tables').where({table_schema: 'btr'});
    assert.equal(_.size(TABLE), _.size(tables));
  });
});

describe(`Exchange`, function () {
  describe(`#.Add Exchanges()`, function () {
    it(`거래소 추가`, async function () {
      await assert.doesNotReject(btr.Exchange.add("업비트", "UPBIT"));
      await assert.doesNotReject(btr.Exchange.add("비트파이낸스", "BITFINANCE"));
      await assert.doesNotReject(btr.Exchange.add("바이낸스", "BINANCE"));
    });
    it(`거래소 추가 확인`, async function () {
      let result = await btr.db.select('*').from(TABLE.EXCHANGE);
      assert.equal(_.size(result), 3);
    });
  });

  describe(`#.Add markets()`, function () {
    it(`마켓 추가`, async function () {
      await assert.doesNotReject(btr.Exchange.addMarket("비트코인", "BTC"));
      await assert.doesNotReject(btr.Exchange.addMarket("원화", "KRW"));
    });
    it(`추가된 마켓 확인 `, async function () {
      let result = await btr.db.select('*').from(TABLE.MARKET);
      assert.equal(_.size(result), 2);
    });
  });

  describe(`#.Add coins()`, function () {
    it(`코인 추가`, async function () {
      for (var i = 0; i < coinList.length; ++i) {
        await assert.doesNotReject(btr.Exchange.addCoin(coinList[i].name, coinList[i].symbol));
      }
    });
    it(`추가된 코인 확인 `, async function () {
      let result = await btr.db.select('*').from(TABLE.COIN);
      assert.equal(_.size(result), coinList.length);
    });
  });

  describe(`#.combine()`, function () {
    it(`거래소 - 마켓 연동`, async function () {
      await assert.doesNotReject(btr.Exchange.combineMarket('UPBIT', 'BTC'));
      await assert.doesNotReject(btr.Exchange.combineMarket('UPBIT', 'KRW'));
    });
    it(`거래소 - 코인 연동`, async function () {
      await assert.doesNotReject(btr.Exchange.combineCoin('UPBIT', 'ETH'));
      await assert.doesNotReject(btr.Exchange.combineCoin('UPBIT', 'ETC'));
    });
    it(`연동 확인`, async function () {
      let markets = await btr.Exchange.getMarketAll('UPBIT');
      assert.equal(_.size(markets), 2);
      let coins = await btr.Exchange.getCoinAll('UPBIT');
      assert.equal(_.size(coins), 2);
    });
  });
});

describe(`#.Uninstall()`, function () {
  it(`Not Reject`, async function () {
    await assert.doesNotReject(btr.uninstall());
  });
  it(`Check dropped tables`, async function () {
    let tables = await btr.db.select('*').from('information_schema.tables').where({table_schema: 'btr'});
    assert.equal(0, _.size(tables));
  });
});

describe(`#.Disconnect()`, function () {
  it(`run`, async function () {
    await assert.doesNotReject(btr.disconnect());
  });
});
