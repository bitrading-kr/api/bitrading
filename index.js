/**
 * @fileOverview
 * @name bitrading api
 * @author https://bitrading.kr
 * @version 1.0.2
 */
"use strict";
const VERSION = "1.0.2";
const mysql = require('mysql'),
      knex = require('knex');
const Init = require('./lib/init/index.js'),
      Auth = require('./lib/Auth'),
      Group = require('./lib/Group'),
      License = require('./lib/License'),
      Order = require('./lib/Order'),
      Trading = require('./lib/Trading'),
      Exchange = require('./lib/Exchange');

class BitradingAPI {
  constructor (host, port, user, password, database) {
    let connection = require('knex')({
      client: 'mysql',
      connection: {
        host : host,
        port: port,
        user : user,
        password : password,
        database : database
      }
    });

    this.db = connection;
    // this.db.connect();

    this._init = null;
    this._user = null;
    this._license = null;
    this._order = null;
    this._exchange = null;
  }

  get version () {
    return VERSION;
  }

  async install () {
    let result;
    try {
      result = await this.Init.init();
    } catch (e) {
      throw e;
    }
    return result;
  }

  async uninstall () {
    let result;
    try {
      result = await this.Init.destory();
    } catch (e) {
      throw e;
    }
    return result;
  }

  async disconnect () {
    this.db.destroy();
  }

  /* Getters */
  get Init () {
    if (this._init == null) {
      this._init = new Init(this.db);
    }
    return this._init;
  }

  get Group () {
    if (this._group == null) {
      this._group = new Group(this.db);
    }
    return this._group;
  }

  get Auth () {
    if (this._auth == null) {
      this._auth = new Auth(this.db);
    }
    return this._auth;
  }
  get License () {
    if (this._license == null) {
      this._license = new License(this.db);
    }
    return this._license;
  }

  get Order () {
    if (this._order == null) {
      this._order = new Order(this.db);
    }
    return this._order;
  }

  get Trading () {
    if (this._trading == null) {
      this._trading = new Trading(this.db);
    }
    return this._trading;
  }

  get Exchange () {
    if (this._exchange == null) {
      this._exchange = new Exchange(this.db);
    }
    return this._exchange;
  }
}

module.exports = BitradingAPI;
